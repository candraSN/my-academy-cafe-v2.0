//
//  FoodClass.swift
//  Academy's Cafe
//
//  Created by Candra Sabdana Nugroho on 07/06/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

class MenuList {
    var menuNameList: String
    var menuIDList: String
    
    init(menuNameList: String, menuIDList: String) {
        self.menuNameList = menuNameList
        self.menuIDList = menuIDList
    }
}
