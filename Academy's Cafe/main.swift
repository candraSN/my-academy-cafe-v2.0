import Foundation

// TODO: You may add/edit/remove any default code here

var foodAndBeverages: [MenuList] = [MenuList(menuNameList: "Nasgor", menuIDList: "f01"), MenuList(menuNameList: "Bakso", menuIDList: "f02"), MenuList(menuNameList: "Sate", menuIDList: "f03"), MenuList(menuNameList: "Es Jeruk", menuIDList: "b01"), MenuList(menuNameList: "Air Putih", menuIDList: "b02")]
var shoppingCart: [ShoppingCartQuantity] = []
var actionsAnswer: String = ""
var menuAnswer: String = ""

func showMainMenu() {
    print("""
    =================================
       Academy's Cafe & Resto v2.0
    =================================
    Options:
    [1] Buy Food
    [2] Shopping Cart
    [x] Exit
    """)
    
    print("Your choice? ", terminator: "")
}

func showListMenu() {
    print("""
        \nHi, we have 5 food and beverages options for you!
        =================================================
        [\(foodAndBeverages[0].menuIDList)] \(foodAndBeverages[0].menuNameList)
        [\(foodAndBeverages[1].menuIDList)] \(foodAndBeverages[1].menuNameList)
        [\(foodAndBeverages[2].menuIDList)] \(foodAndBeverages[2].menuNameList)
        [\(foodAndBeverages[3].menuIDList)] \(foodAndBeverages[3].menuNameList)
        [\(foodAndBeverages[4].menuIDList)] \(foodAndBeverages[4].menuNameList)
        [q] Back to main menu
        """
    )
}

func shoppingCartCount() {
    print("\nShopping Cart (\(shoppingCart.count) items):")
    for i in shoppingCart {
        print("\n\(i.menuQuantity) \(i.menuNameList)")
    }
}

while actionsAnswer.lowercased() != "x" {
    showMainMenu()
    actionsAnswer = readLine() ?? ""
    
    if actionsAnswer == "1" {
        showListMenu()
        
        while menuAnswer.lowercased() != "q" {
            print("\nYour F&B choice?\n")
            menuAnswer = readLine() ?? ""
            
            if menuAnswer.lowercased() != "q" {
                var choiceName: String = ""
                for i in foodAndBeverages {
                    if i.menuIDList == menuAnswer.lowercased() {
                        choiceName = i.menuNameList
                        break
                    }
                }
                let buyItem = ShoppingCartQuantity(menuNameList: choiceName, menuIDList: menuAnswer.lowercased())
                print("\nHow many \(choiceName) you want to buy?\n")
                let amountOfMenu = readLine() ?? "1"
                buyItem.menuQuantity = Int(amountOfMenu) ?? 1
                shoppingCart.append(buyItem)
                shoppingCartCount()
            }
        }
    }
        
    else if actionsAnswer == "2" {
        if shoppingCart.count == 0 {
            print("\nOhhh no.. your cart is empty, please buy something!\n")
        }
        else {
            shoppingCartCount()
        }
    }
}



